package id.ac.ukdw.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val listMusic = arrayListOf(
            MyMusic(1,R.drawable.foto1,"Music 1"),
            MyMusic(2,R.drawable.foto2,"Music 2"),
            MyMusic(3,R.drawable.foto3,"Music 3")
        )

        val adapter = MusicAdapter(listMusic)

        val layoutManager = LinearLayoutManager(this,GridLayoutManager.VERTICAL,false)

        val recyclerView = findViewById<RecyclerView>(R.id.rcyView)

        recyclerView.layoutManager = layoutManager

        recyclerView.adapter = adapter
    }
}